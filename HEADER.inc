<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  
  <head>
    <title>Michel Salim @ Fedora People</title>
    <style type="text/css" media="screen">
      @import url("http://fedorapeople.org/userdefs/css/fedora.css");
      @import url("http://fedorapeople.org/userdefs/css/style.css");
    </style>
  </head>

  <body>

    <div id="wrapper">

      <div id="head">
        <h1><a href="http://fedoraproject.org/">Fedora</a></h1>
      </div>
      <div id="sidebar">
        <div class="nav">
          <h2>Navigation</h2>
          <ul>

            <li><strong><a href="/">Home</a></strong></li>
            <li><strong><a href="/specs/">Packages for review</a></strong>

          </ul>
        </div>
      </div>
      <div id="content">
        <div id="pageLogin">
        </div>


